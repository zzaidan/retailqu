/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,jsx,ts,tsx}",

  ],
  theme: {
    extend: {
      spacing: {
        '400': '400px',
        '610': '610px',
        '500': '500px',
      },
      fontFamily: {
        'roboto': 'Roboto',
        'roboto-condensed': 'Roboto Condensed',
        'playfair': 'Playfair Display',
        'roboto-flex': 'Roboto Flex',
      },
      objectPosition: {
        'center-left': 'center left',
      }
    },
  },
  plugins: [],
}

