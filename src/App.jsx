/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-vars */
import { useState } from "react";
import "./App.css";
import Logo from "./assets/img/retailqu-logo.png";
import Awalan from "./assets/img/awalan.jpg";
import Warnu from "./assets/img/warnu.png";
import Beautyloca from "./assets/img/beautyloca-logo.png";
import Nasikebuli from "./assets/img/nasi-kebuli.png";
import Alyasin from "./assets/img/alyasin.png";
import Stempos from "./assets/img/stempos.png";
import Kasir from "./assets/img/kasir.jpg";
import Perdagangan from "./assets/img/perdagangan.jpg";
import Manajemen from "./assets/img/manajemen.jpg";
import Kasirdigital from "./assets/img/kasir-digital.jpg";
import Dashboardpintar from "./assets/img/dashboard-pintar.jpg";
import Sistemgudang from "./assets/img/sistem-gudang.jpg";
import Restoran from "./assets/img/restoran.jpg";
import Kedaikopi from "./assets/img/coffee-shop.jpg";
import Makanan from "./assets/img/makanan.jpg";
import Minuman from "./assets/img/minuman.jpg";
import Laundry from "./assets/img/laundry.jpg";
import Petshop from "./assets/img/petshop.jpg";
import Alldevice from "./assets/img/all-device.png";
import Android from "./assets/img/android.png";
import Apple from "./assets/img/apple.png";
import Windows from "./assets/img/windows.png";
import Kasironline from "./assets/img/kasir-online.png";
import Realtime from "./assets/img/realtime.png";
import Manajemenmember from "./assets/img/manajemen-member.png";
import Laporankeuangan from "./assets/img/laporan-keuangan.png";
import Multicabang from "./assets/img/multi-cabang.png";
import Pembayarandigital from "./assets/img/pembayaran-digital.png";
import Sistemkepegawaian from "./assets/img/sistem-kepegawaian.png";
import Onlinestore from "./assets/img/online-store.png";
import Statistikstok from "./assets/img/statistik-stok.png";
import Garis from "./assets/img/garis.png";
import Qr from "./assets/img/qr.png";
import Bri from "./assets/img/bri.png";
import Bni from "./assets/img/bni.png";
import Bca from "./assets/img/bca.png";
import Mandiri from "./assets/img/bank-mandiri.png";
import Ocbc from "./assets/img/bank-ocbc.png";
import Paninbank from "./assets/img/bank-paninbank.png";
import Permata from "./assets/img/bank-permata.png";
import Telephone from "./assets/img/telephone.jpg";

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <div className="flex flex-col w-full h-[1250px] bg-cover bg-center mb-[70px] md:h-[750px] md:flex-row bg-[url(./assets/img/backgroud.jpg)]">
        <div className="md:w-[1150px] md:flex md:mx-auto">
          <div className="md:flex md:flex-col">
            <div className="flex flex-col mt-[30px] md:w-[490px]">
              <img className="w-[120px] h-[120px] mx-auto mb-[30px]" src={Logo} alt="Logo Retailqu" />
              <h3 className="font-roboto-condensed text-4xl text-white font-bold w-[370px] text-center mx-auto mb-6 md:text-5xl md:text-left md:w-[500px]">Mari Kelola Retail Anda <span className="text-red-800">Secara Praktis & Modern.</span></h3>
              <p className="font-roboto-flex text-white text-lg w-[400px] mx-auto text-justify mb-[20px] md:text-left md:w-[480px] md:text-xl md:ml-0">
              Kini mengelola bisnis retail Anda begitu mudah. Segala Aktivitas Usaha mulai daripergudangan (Purchase Order, Recipt Order).
              </p>
              <p className="font-roboto-flex text-white text-lg w-[400px] mx-auto text-justify mb-[20px] md:text-left md:w-[480px] md:text-xl md:ml-0">Transaksi dengan Point of Sale yang canggih, hingga satu Dashboard berisikan segala Business Insight yang anda perlukan untuk memajukan Usaha Anda, Ada di RetailQu.</p>
            </div>
            <div className="w-full flex flex-col h-[100px] mx-auto mb-6 md:flex-row md:w-[480px] md:ml-0 md:mb-0 md:h-[60px] ">
              <button className="w-[400px] border-2 rounded-xl border-[#EC3030] mx-auto p-3 mb-3 hover:bg-[#EA2020] md:w-[230px] md:ml-0 md:h-[60px]"><span className="text-white font-roboto-flex">Coba Sistem Kasir</span></button>
              <button className="w-[400px] bg-[#EC3030] mx-auto rounded-xl p-3 hover:bg-[#EA2020] md:w-[230px] md:h-[60px]"><span className="text-white font-roboto-flex text-md">Coba Dashboard Bisnis</span></button>
            </div>
            <div className="w-[350px] mx-auto mb-10 md:mt-3">
              <center><a className="text-sm font-roboto md:text-md text-slate-200" href="#"><u>Atau hubungi sales kami via WhatsApp</u></a></center>
            </div>
          </div>
          <div className="w-[350px] h-[470px] mx-auto md:mr-0 md:mt-[60px] md:w-[450px]">
            <img className="w-[350px] h-[470px] object-cover rounded-3xl md:w-[450px] md:h-[550px]" src={Awalan} alt="Awalan" />
          </div>
        </div>
      </div>

    {/* Jarak */}
      
      <div className="w-full mb-[100px]">
        <p className="text-center font-roboto-flex text-lg mb-[50px]">TELAH DIGUNAKAN OLEH</p>
        <div className="w-[160px] space-y-8 mx-auto flex flex-wrap justify-evenly md:w-[1150px] md:space-y-0 md:justify-between md:flex-nowrap">
          <img className="w-[150px] h-[50px] md:w-[180px] md:h-[60px]" src={Warnu} alt="/" />
          <img className="w-[150px] h-[50px] md:w-[180px] md:h-[60px]" src={Nasikebuli} alt="/" />
          <img className="w-[150px] h-[50px] md:w-[185px] md:h-[60px]" src={Alyasin} alt="/" />
          <img className="w-[110px] h-[50px] md:w-[135px] md:h-[60px]" src={Stempos} alt="/" />
          <img className="w-[90px] h-[70px] md:w-[90px] md:h-[80px]" src={Beautyloca} alt="/" />
        </div>
      </div>


      {/* <div className="relative flex items-center">
        {data.map((item)=> (
          <img className="inline-block p-2 w-[220px] h-[140px]" src={item.img} alt='/' />
        ))} 
      </div> */}

    {/* Jarak */}

      <div className="w-full mb-[40px]">
        <h1 className="text-center font-roboto-condensed font-bold text-3xl md:text-5xl">Lebih dari <span className="text-[#FF0000]">Point of Sale</span></h1>
      </div>

    {/* Jarak */}


      <div className="flex h-full flex-wrap mb-[140px] md:justify-around md:flex-nowrap relative md:mb-[100px]">
        <div className="w-[500px] h-[180px] mb-[30px] flex flex-row mx-auto bg-white rounded-3xl shadow-2xl cursor-pointer md:mx-0 md:w-[380px] md:h-[530px] md:overflow-y-hidden md:flex md:flex-col">
          <img
            className="w-[230px] h-[180px] rounded-tl-3xl rounded-bl-3xl relative object-cover md:w-[382px] md:h-[255px] md:hover:scale-105 md:transition-all md:duration-200 md:rounded-tl-3xl md:rounded-tr-3xl md:rounded-bl-none"
            src={Kasir}
            alt="Kasir"
          />
          <p className="w-[70px] bg-[#AB9C9C] text-center p-1 font-roboto-condensed rounded-xl text-sm absolute top-0 mt-[15px] ml-[195px] md:text-lg md:mt-[235px] md:ml-[40px]">KASIR</p>
          <div>
            <h3 className="text-lg text-center mt-[10px] font-bold sm:text-3xl sm:mt-[30px]">
              Sistem Kasir
            </h3>
            <p className="text-md text-center mx-7 mt-[15px] space-x-2 sm:text-lg sm:mt-[25px]">
              Sistem Kasir Digital terintegrasi Gudang Anda serta berbagai
              Layanan Keuangan & Pembayaran.
            </p>
          </div>
        </div>
        <div className="w-[500px] h-[180px] mb-[30px] flex flex-row mx-auto bg-white rounded-3xl shadow-2xl cursor-pointer md:mx-0 md:w-[380px] md:h-[530px] md:overflow-y-hidden md:flex md:flex-col relative">
          <img
            className="w-[230px] h-[180px] rounded-tl-3xl rounded-bl-3xl relative object-cover md:w-[382px] md:h-[255px] md:rounded-tl-3xl md:rounded-tr-3xl md:rounded-bl-none md:hover:scale-105 md:transition-all md:duration-200"
            src={Perdagangan}
            alt="Perdagangan"
          />
          <p className="w-[110px] bg-[#AB9C9C] text-center font-roboto-condensed rounded-xl text-sm absolute top-0 p-1 mt-[15px] ml-[175px] md:text-lg md:mt-[235px] md:ml-[40px] md:w-[130px]">PERDAGANGAN</p>
          <div>
            <h3 className="text-lg text-center mt-[10px] font-bold md:text-3xl md:mt-[30px]">
              Sistem Stok
            </h3>
            <p className="text-md text-center mx-7 mt-[15px] space-x-2 md:text-lg md:mt-[25px]">
              Pantau kondisi stok barang yang terhubung dengan Sistem Kasir &
              Sistem Order.
            </p>
          </div>
        </div>
        <div className="w-[500px] h-[180px] mb-[20px] flex flex-row mx-auto bg-white rounded-3xl shadow-2xl cursor-pointer md:mx-0 md:w-[380px] md:h-[530px] md:overflow-y-hidden  md:flex md:flex-col relative">
          <img
            className="w-[230px] h-[180px] rounded-tl-3xl rounded-bl-3xl relative object-cover md:w-[382px] md:h-[255px] md:rounded-tl-3xl md:rounded-tr-3xl md:rounded-bl-none md:group-hover:scale-105 md:transition-all md:duration-200"
            src={Manajemen}
            alt="Manajemen"
          />
          <p className="w-[95px] bg-[#AB9C9C] text-center font-roboto-condensed rounded-xl text-sm absolute top-0 p-1 mt-[15px] ml-[185px] md:text-lg md:mt-[235px] md:ml-[40px] md:w-[120px]">MANAJEMEN</p>
          <div>
            <h3 className="text-lg text-center mt-[10px] font-bold md:text-3xl md:mt-[30px]">
              Dashboard
            </h3>
            <p className="text-md text-center mx-7 mt-[15px] space-x-2 md:text-lg md:mt-[25px]">
              Pantau & Pelajari segala aktivitas usaha anda dengan mudah untuk
              kinerja bisnis anda yang lebih baik.
            </p>
          </div>
        </div>
      </div>

      {/* Jarak */}

      <div className="w-full mb-[400px] md:mb-[180px]">
        <h1 className="font-roboto-condensed text-4xl font-bold text-center mb-3 md:text-5xl">Outlet <span className="text-[#FF782D]">Kami</span></h1>
        <p className="text-center font-roboto-flex w-[420px] mx-auto text-lg mb-[30px] md:text-2xl md:w-[800px]">Terpercaya untuk usaha outlet-outlet ritel dan makanan/minuman seperti restoran, kedai kopi, stand makanan/minuman cepat saji, laundry dan pet store yang membutuhkan pencatatan penjualan dan inventori yang tepat akurat.</p>

        <div className="flex w-[480px] h-[250px] mx-auto justify-between flex-wrap md:w-[1000px] md:h-[500px] md:overflow-hidden">
          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl md:overflow-hidden">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Restoran</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Restoran} alt="Restoran" />
          </div>

          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Kedai Kopi</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Kedaikopi} alt="Kedaikopi" />
          </div>

          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Makanan</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Makanan} alt="Makanan" />
          </div>

          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Minuman</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Minuman} alt="Minuman" />
          </div>

          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Laundry</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Laundry} alt="Laundry" />
          </div>

          <div className="relative cursor-pointer mb-5 md:h-[190px] md:rounded-2xl">
            <div className="absolute w-full h-[150px] rounded-2xl bg-black/30 flex items-center justify-center md:h-full md:justify-center md:items-center md:rounded-2xl md:bg-black/30 md:-bottom-5 md:hover:bottom-0 md:opacity-0 md:hover:opacity-100 md:transition-all md:duration-200">
              <p className="text-white font-roboto-condensed text-3xl font-bold md:text-4xl">Pet Shop</p>
            </div>
            <img className="w-[230px] h-[150px] object-cover rounded-2xl md:w-[300px] md:h-[190px]" src={Petshop} alt="Petshop" />
          </div>

        </div>
      </div>

      {/* Jarak */}

      <div className="w-full h-[630px] flex flex-col bg-[#FFEDDD] md:h-[460px] md:flex-row">
        <div className="md:w-[1150px] md:mx-auto md:flex md:flex-row ">
          <img className="w-[330px] h-[230px] object-cover rounded-xl mx-auto mt-14 mb-10 md:ml-0 md:my-auto md:w-[460px] md:h-[330px]" src={Kasirdigital} alt="Kasir Digital" />
          <div className="md:flex md:flex-col md:mr-0 md:my-auto">
            <h4 className="font-roboto-condensed text-4xl w-[400px] text-center mx-auto font-bold mb-6 md:text-5xl md:w-[550px] md:text-right">Kasir Digital yang Canggih <span className="text-[#FF7E07]">untuk Segala Alat Bayar</span></h4>
            <p className="font-roboto-flex w-[350px] text-center mx-auto text-2xl md:w-[450px] md:text-3xl md:text-right md:mr-0">Sistem Kasir terintegrasi dengan Gudang Anda serta telah terintegrasi dengan jaringan QRIS & Perbankan di Indonesia.</p>
          </div>
        </div>
      </div>

      <div className="w-full h-[630px] flex flex-col md:h-[460px] md:flex-row">
        <div className="md:w-[1150px] md:mx-auto md:flex md:flex-row">
          <div className="md:flex md:order-2 md:ml-[260px]">
            <img className="w-[330px] h-[230px] object-cover rounded-xl mx-auto mt-14 mb-10 md:w-[460px] md:h-[330px]" src={Dashboardpintar} alt="Dashboard Pintar" />
          </div>
          <div className="md:flex md:flex-col md:mr-0 md:my-auto md:order-1">
            <h4 className="font-roboto-condensed text-4xl w-[400px] text-center mx-auto font-bold mb-6 md:text-5xl md:text-left md:w-[450px]">Dashboard pintar untuk <span className="text-[#21D804]">Seluruh aktivitas usaha</span></h4>
            <p className="font-roboto-flex w-[350px] text-center mx-auto text-2xl md:text-3xl md:text-left md:ml-0 md:w-[400px]">Sistem Dashboard All-in-One menyajikan seluruh manajemen usaha Anda mulai dari penjualan hingga kepegawaian.</p>
          </div>
        </div>
      </div>

      <div className="w-full h-[630px] bg-[#FFEDDD] flex flex-col mb-[230px] md:h-[460px] md:flex-row">
        <div className="md:w-[1150px] md:mx-auto md:flex md:flex-row">
          <img className="w-[330px] h-[230px] object-cover rounded-xl mx-auto mt-14 mb-10 md:my-auto md:w-[460px] md:ml-0 md:h-[330px]" src={Sistemgudang} alt="Sistem Gudang" />
          <div className="md:flex md:flex-col md:my-auto md:ml-0">
            <h4 className="font-roboto-condensed text-4xl w-[400px] text-center mx-auto font-bold mb-6 md:text-5xl md:text-right md:mr-0 md:w-[500px]">Sistem Gudang untuk <span className="text-[#6C6655]">Manajemen Arus Barang</span></h4>
            <p className="font-roboto-flex w-[350px] text-center mx-auto text-2xl md:text-3xl md:mr-0 md:text-right md:w-[450px]">Sistem Manajemen Gudang dapat memantau seluruh aliran keluar-masuk barang baik karena stok opname, penjualan, hingga penerimaan barang dari supplier.</p>
          </div>
        </div>
      </div>

      {/* Jarak */}

      <div className="w-full h-[630px] flex flex-col bg-[#F3ECEC] mb-[200px] md:h-[460px]">
        <div className="md:w-[1150px] md:h-full md:mx-auto md:flex md:flex-row md:justify-between">
          <div className="md:order-2 md:my-auto">
            <img className="w-[250px] h-[250px] mx-auto mt-[20px] md:my-auto md:w-[380px] md:h-[370px] md:right-0" src={Alldevice} alt="All Device" />
          </div>
          <div className="md:flex md:flex-col md:my-auto md:ml-0 md:order-1">
            <h3 className="text-3xl font-bold font-roboto-condensed w-[350px] mx-auto text-center mb-5 md:text-5xl md:w-[550px] md:text-left">Dapat Diakses di Manapun <span className="text-[#D04B00]"> Dan di Perangkat Manapun</span></h3>
            <p className="text-xl font-roboto-flex text-center w-[360px] mx-auto mb-5 md:text-2xl md:w-[450px] md:ml-0 md:text-left md:mt-2">Sebuah Web yang bisa diakses di perangkat manapun, memiliki berbagai macam fitur pada setiap perangkat, mulai dari Desktop PC, Laptop, Smartphone Android, hingga iPad dan iPhone.</p>
            <div className="flex w-[250px] justify-evenly mx-auto md:ml-0 md:justify-between">
              <img className="w-[52px] h-[52px] md:w-[60px] md:h-[60px]" src={Android} alt="Android" />
              <img className="w-[52px] h-[52px] md:w-[60px] md:h-[60px]" src={Apple} alt="Apple" />
              <img className="w-[52px] h-[52px] md:w-[60px] md:h-[60px]" src={Windows} alt="Windows" />
            </div>
          </div>
        </div>
      </div>

      {/* Jarak */}

      <div className="w-full">
        <h3 className="text-center text-4xl font-roboto-condensed font-bold mb-10">Fitur Fitur RetailQu</h3>
      </div>

      {/* <div className="w-[1150px] mx-auto mb-[180px] relative">
        <div className="w-[350px] flex absolute right-0 justify-between">
          <button className="px-16 py-2 rounded-xl border border-black/20">Geser</button>
          <button className="px-16 py-2 rounded-xl border border-black/20">Geser</button>
        </div>
        <p className="w-[1150px] mx-auto text-4xl font-roboto-condensed font-bold mb-20">Fitur - Fitur RetailQu</p>
        <div className="flex overflow-hidden space-x-[84px]">
          <div className="w-[350px] h-[410px] border border-black/30 rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Kasironline} alt="Kasir Online To Offline" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Kasir Online</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Miliki alat kasir yang terintegrasi <span className="italic">Cashless System & Loyalty Program.</span></p>
            <center><button className="mt-[35px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Realtime} alt="Realtime Transaction" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Realtime Transaction</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Bayar secara langsung (Cash on Delevery)</p>
            <center><button className="mt-[64px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Manajemenmember} alt="Manajemen Member" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Manajemen Member</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Memanaje berbagai hal seputar member dengan akurat.</p>
            <center><button className="mt-[64px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Laporankeuangan} alt="Laporan Keuangan" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Laporan Keuangan</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Akses berbagai laporan terkait penjualan, stok, hingga keuangan.</p>
            <center><button className="mt-[64px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Multicabang} alt="Manajemen Multi Cabang dan Gudang" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[15px] ml-[25px] text-3xl font-bold">Manajemen Multi Cabang dan Gudang</p>
            <p className="mt-[15px] ml-[25px] w-[300px] text-xl">Gunakanlah untuk mengelola produk, melakukan PO atau menerima RO.</p>
            <center><button className="mt-[15px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Laporankeuangan} alt="Laporan Keuangan" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[15px] ml-[25px] text-3xl font-bold">Agen Pembayaran Digital</p>
            <p className="mt-[15px] ml-[25px] w-[300px] text-xl">-</p>
            <center><button className="mt-[64px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Sistemkepegawaian} alt="Agen Pembayaran Digital" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Sistem Kepegawaian</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Kelola pegawaimu dalam satu sistem yang sama dengan usahamu.</p>
            <center><button className="mt-[35px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Onlinestore} alt="Sinkronisasi Online Store" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[15px] ml-[25px] text-3xl font-bold">Sinkronisasi Online Store</p>
            <p className="mt-[15px] ml-[25px] w-[300px] text-xl">Bebas dari kehilangan data bisnismu karena bencana atau perampokan.</p>
            <center><button className="mt-[15px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
          <div className="w-[350px] h-[410px] border border-black/30 shadow rounded-2xl font-roboto-flex">
            <img className="ml-[30px] mt-[35px] mb-7" src={Statistikstok} alt="Statistik Stok (Persediaan)" />
            <center><img src={Garis} alt="Garis" /></center>
            <p className="mt-[20px] ml-[25px] text-3xl font-bold">Statistik Stok</p>
            <p className="mt-[20px] ml-[25px] w-[300px] text-xl">Fitur stok yang berguna untuk menganalisis berbagai macam stok yang ada.</p>
            <center><button className="mt-[35px] px-[85px] py-[10px] text-white text-roboto rounded-lg bg-[#FF0000] hover:bg-[#E02020]">Pelajari lebih lanjut</button></center>
          </div>
        </div>
      </div> */}

      <div className="mb-20 md:w-[1150px] md:h-[600px] md:mx-auto md:flex md:justify-between md:flex-wrap md:mb-[520px]">
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Kasironline} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Kasir Online to Offline</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Miliki alat kasir yang terintegrasi <span className="italic">Cashless System & Loyalty Program</span></p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Realtime} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Realtime Transaction</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Bayar Secara Langsung (Cash on Delevery)</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Manajemenmember} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Manajemen Member</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Memanaje berbagai macam hal seputar member dengan akurat</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Laporankeuangan} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Laporan Keuangan</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Akses berbagai laporan terkait penjualan, stok, hingga keuangan</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
           <center><img className="mb-5" src={Multicabang} alt="/" /></center>
           <h4 className="text-center text-2xl  text-gray-900 font-semibold mb-3">Manajemen Multi Cabang & Gudang</h4>
           <p className="text-center text-xl  text-gray-700font-roboto-flex">Gunakanlah untuk mengelola stok produk, melakukan PO/menerima PO</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Pembayarandigital} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Agen Pembayaran Digital</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Tingkatkan keuntungan dengan usaha PPOB & Layanan Keuangan RetailQu.</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
           <center><img className="mb-5" src={Sistemkepegawaian} alt="/" /></center>
           <h4 className="text-center text-2xl  text-gray-900 font-semibold mb-3">Sistem Kepegawaian</h4>
           <p className="text-center text-xl  text-gray-700font-roboto-flex">Kelola pegawaimu dalam satu sistem yang sama dengan usahamu</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mb-8 mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
           <center><img className="mb-5" src={Onlinestore} alt="/" /></center> 
           <h4 className="text-center text-2xl  text-gray-900 font-semibold mb-3">Sinkronisasi Online Store</h4>
           <p className="text-center text-xl  text-gray-700font-roboto-flex">Bebas dari kehilangan data bisnismu karena bencana atau perampokan</p>
          </div>
        </div>
        <div className="bg-gray-200 flex items-center w-[480px] h-[230px] mx-auto md:w-[360px] md:h-[280px] md:m-0 md:mb-9 md:rounded-xl md:items-start">
          <div className="h-[200px] w-[400px] mx-auto md:w-[330px] md:mt-5">
            <center><img className="mb-5" src={Statistikstok} alt="/" /></center>
            <h4 className="text-center text-2xl text-gray-900 font-semibold mb-3">Statistik Stok (Persediaan)</h4>
            <p className="text-center text-xl text-gray-700 font-roboto-flex">Fitur stok yang berguna untuk menganalisis berbagai macam stok yang ada</p>
          </div>
        </div>
      </div>

      {/* Jarak */}

      <div className="w-full mb-[150px]">
        <div className="md:flex md:flex-row md:w-[1150px] md:h-full md:mx-auto">
          <img className="w-[230px] h-[230px] mx-auto mb-8 md:ml-0 md:w-[300px] md:h-[300px]" src={Qr} alt="Qr" />
          <div className="md:flex md:flex-col md:mr-0">
            <h1 className="w-[300px] text-3xl text-center mx-auto font-roboto-condensed mb-10 md:text-4xl"><span className="text-[#DC3E3E]">Satu QR Code Untuk </span>Semua Pembayaran</h1>
            <div className="flex flex-wrap w-[460px] justify-evenly mx-auto">
              <img className="w-[100px] h-[45px]" src={Bni} alt="Bank Bri" />
              <img className="w-[100px] h-[40px]" src={Bca} alt="Bank Bca" />
              <img className="w-[60px] h-[60px]" src={Bri} alt="Bank Bri" />
              <img className="w-[110px] h-[50px]" src={Mandiri} alt="Bank Mandiri" />
              <img className="w-[120px] h-[40px] mt-6" src={Ocbc} alt="Bank Ocbc Nisp" />
              <div className="w-[120px] h-[40px]"></div>
              <img className="w-[160px] h-[110px] absolute mt-[40px] mr-4" src={Permata} alt="Bank Permata" />
              <img className="w-[150px] h-[90px] mt-" src={Paninbank} alt="Bank Paninbank" />
            </div>
            <center><button className="w-[430px] p-[15px] bg-[#EE4848] rounded-md hover:bg-[#EA2020]"><p className="font-roboto-flex text-white">Metode Lainnya</p></button></center>
          </div>
        </div>
      </div>

      {/* Jarak */}

      <div className="w-full relatiev flex flex-col h-[280px] bg-[#F8673A] mb-[100px] md:w-[1150px] md:h-[350px] md:mx-auto md:rounded-2xl">
        <div className="md:w-[750px] md:h-[350px] md:rounded-tr-2xl md:rounded-br-2xl md:ml-auto">
          <h4 className="mt-8 pr-3 text-right font-roboto-condensed text-2xl text-white font-bold mb-2 md:text-center md:mb-4 md:text-5xl">Mari Pacu Kinerja Usahamu</h4>
          <p className="w-[300px] mr-0 pr-3 text-right mx-auto font-roboto-flex text-lg text-white mb-4 md:text-3xl md:mx-auto md:text-center md:w-[550px]">Produktivitas dan Kemampuan Usahamu melesat maju bersama RetailQu</p>
          <div className="flex justify-end pr-3">
            <button className=" text-white bg-[#48B814] border-2  mr-0 py-[10px] px-[60px] rounded-md hover:bg-[#2F7A0C] md:mt-7 md:block md:mx-auto md:px-[150px] md:py-[15px] md:text-center"><p className="md:w-[150px] md:text-xl mx-auto">WhatsApp Kami</p></button>
          </div>
        </div>
        <img className="absolute w-[225px] h-[280px] object-cover object-left md:w-[400px] md:h-[350px] md:rounded-tl-2xl md:rounded-bl-2xl" src={Telephone} alt="Pacu Usahamu" />
      </div>

      {/* Jarak */}

      <hr className="my-8 border-2" />

      {/* Jarak */}

      <img className="h-[200px] w-[200px]  mx-auto" src={Logo} alt="Logo RetailQu" />
      <p className="text-center text-gray-500 mb-10 mt-4">Hak Cipta & Intelektual © PT Solusi Infotech Semesta Indonesia - 2021</p>
    
    </>
  );
}

export default App;
