import Warnu from "./assets/img/warnu.png";
import Beautyloca from "./assets/img/beautyloca-logo.png";
import Nasikebuli from "./assets/img/nasi-kebuli.png";
import Alyasin from "./assets/img/alyasin.png";
import Stempos from "./assets/img/stempos.png";

export const data = [
    {
        id: 1,
        img: Warnu,
    },
    {
        id: 2,
        img: Beautyloca,
    },
    {
        id: 3,
        img: Nasikebuli,
    },
    {
        id: 4,
        img: Alyasin,
    },
    {
        id: 5,
        img: Stempos,
    }
]
